#!/bin/bash
# shellcheck disable=SC2154

set -euo pipefail

read -ra RPM_PACKAGES_array <<< "${RPM_PACKAGES}"
read -ra CONFIGURE_array <<< "${CONFIGURE}"

echo "Building ${SOURCE_URL}"

CURLRC_FILE=/tmp/curlrc
curl \
    --location \
    --silent \
    --fail \
    --show-error \
    https://gitlab.com/cki-project/containers/-/raw/production/files/curlrc \
    --output "${CURLRC_FILE}"

rm --verbose --force /etc/yum.repos.d/redhat.repo
cp --verbose "${RHEL_VERSION}.repo" /etc/yum.repos.d/

curl \
    --config "${CURLRC_FILE}" \
    --output /etc/pki/ca-trust/source/anchors/rh-root.crt \
    https://gitlab.com/cki-project/containers/-/raw/production/files/rh-root.crt
update-ca-trust

if [[ "${RHEL_VERSION}" == "rhel6" ]]; then
    yum \
    -y \
    install \
    https://archives.fedoraproject.org/pub/archive/epel/6/x86_64/Packages/e/epel-release-6-8.noarch.rpm
fi

yum -y install gcc make tar "${RPM_PACKAGES_array[@]}"
for backport in sqlite openssl gcc; do
    if [[ -f ${backport}.tgz ]]; then
        tar \
            --extract \
            --directory /usr/local/ \
            --file "${backport}.tgz"
        echo "/usr/local/lib64/" > /etc/ld.so.conf.d/local-lib64.conf
        echo "/usr/local/lib/" > /etc/ld.so.conf.d/local-lib.conf
        ldconfig
    elif [[ "${BACKPORT_PACKAGES:-}" == *${backport}* ]]; then
        yum -y install "${backport}-devel"
    fi
done

mkdir src
curl \
    --config "${CURLRC_FILE}" \
    --output src.tar.gz \
    "${SOURCE_URL}"
tar \
    --extract \
    --strip-components 1 \
    --directory src \
    --file src.tar.gz

cd src
"${CONFIGURE_array[@]}"
make
make install

tar \
    --create \
    --gzip \
    --directory "${ARTIFACT_SRC}/" \
    --file "${CI_PROJECT_DIR}/${CI_JOB_NAME}.tgz" \
    .
